package model;

public class ReadLater {
	// Attributes
	private int id;
	private int bookId;
	private int userId;

	// constructor without parameter
	public ReadLater() {

	}

	// parameterized constructor
	public ReadLater(int id, int bookId, int userId) {
		this.id = id;
		this.bookId = bookId;
		this.userId = userId;
	}

	// parameterized constructor with different parameter
	public ReadLater(int bookId, int userId) {
		super();
		this.bookId = bookId;
		this.userId = userId;
	}

	// getter and setter
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "ReadBook [id=" + id + ", bookId=" + bookId + ", userId=" + userId + "]";
	}

}

