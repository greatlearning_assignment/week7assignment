package model;

public class LikedBook {
	// Attributes
	private int id;
	private int bookId;
	private int userId;

	// constructor without parameter
	public LikedBook() {
		super();
	}

	// parameterized constructor
	public LikedBook(int bookId, int userId) {
		super();
		this.bookId = bookId;
		this.userId = userId;
	}

	// parameterized constructor with different parameter
	public LikedBook(int id, int bookId, int userId) {
		super();
		this.id = id;
		this.bookId = bookId;
		this.userId = userId;
	}

	// getter and setter
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "LikedBook [id=" + id + ", bookId=" + bookId + ", userId=" + userId + "]";
	}

}
