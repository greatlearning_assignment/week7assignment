package database;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Book;

public class BookDatabase {
	// Call database connection method 
	private static Connection con = DBConnection.getConnection();

	// This function used for getting all book data
	public List<Book> getAllBooks() throws SQLException {
		List<Book> books = new ArrayList<Book>();
		//Preparing query and executing
		String sql = "select book_id,book_name,book_author,book_price from book";
		// 3) statement
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		//iterating data
		while (rs.next()) {
			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setName(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setPrice(rs.getDouble(4));
			books.add(book);
		}
		return books;
	}

}
