package database;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.LikedBook;
import model.User;

public class LikedBookDatabase {
	// Calling databse connection method
	private static Connection con = DBConnection.getConnection();

	// this function is used for the getting liked book list
	public List<Book> getAllLikedBook(int userId) throws SQLException {
		List<Book> books = new ArrayList<Book>();

		// preparing query and executing it
		String sql = "select book.book_id,book.book_name,book_author,book.book_price" + " from"
				+ " likebook" + " left join book ON(book.book_id = likebook.book_id)"
				+ " left join user ON(user.id = likebook.user_id)" + " Where user.id = ?";

		PreparedStatement statement = con.prepareStatement(sql);
		statement.setInt(1, userId);
		ResultSet rs = statement.executeQuery();
		// Iterating data
		while (rs.next()) {

			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setName(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setPrice(rs.getDouble(4));
			books.add(book);
		}
		return books;
	}

	// This function is used for the insert book into liked book list
	public boolean insertLikeBook(LikedBook likedBook) throws SQLException {
		// call the function which check book is already exist or not
		if (validateExistingBookList(likedBook)) {
			return false;
		} else {
			// preparing query and executing it
			String insertQuery = "insert into likebook(book_id,user_id) values(?,?)";
			PreparedStatement ps = con.prepareStatement(insertQuery);

			ps.setInt(1, likedBook.getBookId());
			ps.setInt(2, likedBook.getUserId());
			ps.executeUpdate();
			return true;
		}
	}

	// This function is used for the checking book is already exist in the liked
	// book list
	public boolean validateExistingBookList(LikedBook likedBook) throws SQLException {
		String sql = "select * from likebook";
		Statement statement = con.createStatement();

		// Executing query and getting the data
		ResultSet result = statement.executeQuery(sql);
		while (result.next()) {
			if (likedBook.getBookId() == result.getInt(2) && likedBook.getUserId() == result.getInt(3)) {
				return true;
			}
		}
		return false;
	}
}

