package database;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.User;

public class UserDatabase {
	// Calling the db connection function
	private static Connection con = DBConnection.getConnection();

	// This function used for validating the user at the login time
	public boolean validate(User user) throws SQLException {
		// Write the query for select all email id's and password of all users
		String sqlQuery = "Select user_email, user_password from user";
		Statement statement = con.createStatement();

		// Executing query and getting the data
		ResultSet result = statement.executeQuery(sqlQuery);
		while (result.next()) {
			// validating user
			if (user.getEmail().equals(result.getString(1)) && user.getPassword().equals(result.getString(2))) {
				return true;
			}
		}
		return false;
	}

	public boolean userRegistration(User user) throws SQLException {
		if (userExistValidation(user)) {
			return false;
		} else {
			// Preparing query and executing it
			String insertQuery = "insert into user(user_id,user_name,user_email,user_password) values(?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(insertQuery);
			ps.setInt(1, user.getId());
			ps.setString(2, user.getName());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getPassword());
			ps.executeUpdate();
			System.out.println("Registraion processing");
			return true;
		}
	}
	public boolean userExistValidation(User user) throws SQLException {
		String sqlQuery = "Select user_email from user";
		Statement statement = con.createStatement();

		ResultSet result = statement.executeQuery(sqlQuery);
		while (result.next()) {
			// validating user
			System.out.println("Validation user");
			if (user.getEmail().equals(result.getString(1))) {
				return true;
			}
		}
		return false;
	}

	// This function is used for getting user by email id
	public User getUserNameByEmail(String email) throws SQLException {
		User user = new User();

		// preparing query and execute it
		String sql = "select user_id, user_name from user" + " where user_email=?";

		PreparedStatement statement = con.prepareStatement(sql);
		statement.setString(1, email);
		ResultSet rs = statement.executeQuery();
		while (rs.next()) {
			user.setId(rs.getInt(1));
			user.setName(rs.getString(2));
		}
		return user;
	}

}
