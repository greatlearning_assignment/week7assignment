package database;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.ReadLater;

public class ReadBookDatabase {
	// Calling the database connection method
	private static Connection con = DBConnection.getConnection();

	// This function is used for getting the list of read later books
	public List<Book> getAllLaterReadBook(int userId) throws SQLException {
		List<Book> books = new ArrayList<Book>();

		// Preparing query and execute it
		String sql = "select book.book_id,book.book_name,book.book_author,book.book_price" + " from"
				+ " readlater" + " left join tbl_book ON(book.book_id = readlater.book_id)"
				+ " left join user ON(user.user_id = readlater.user_id)" + " Where user.user_id = ?";

		PreparedStatement statement = con.prepareStatement(sql);
		statement.setInt(1, userId);
		ResultSet rs = statement.executeQuery();
		// Iterating data
		while (rs.next()) {
			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setName(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setPrice(rs.getDouble(4));
			books.add(book);
		}
		return books;
	}

	// This function is used for insert book into read later list
	public boolean insertReadLater(ReadLater readBook) throws SQLException {
		// checking book is already in list
		if (validateExistingReadBookList(readBook)) {
			return false;
		} else {
			// Preparing query and execute it
			String insertQuery = "insert into readlater(book_id,user_id) values(?,?)";
			PreparedStatement ps = con.prepareStatement(insertQuery);

			ps.setInt(1, readBook.getBookId());
			ps.setInt(2, readBook.getUserId());
			ps.executeUpdate();
			return true;
		}
	}

	// This function is used for checking the book is already exist in list.
	public boolean validateExistingReadBookList(ReadLater readBook) throws SQLException {
		String sql = "select * from readlater";
		Statement statement = con.createStatement();

		// Executing query and getting the data
		ResultSet result = statement.executeQuery(sql);
		while (result.next()) {
			if (readBook.getBookId() == result.getInt(2) && readBook.getUserId() == result.getInt(3)) {
				return true;
			}
		}
		return false;
	}
}

