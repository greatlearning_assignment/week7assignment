package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

import database.LikedBookDatabase;
import model.Book;
import model.LikedBook;

/**
 * Servlet implementation class LikedBookServlet
 */
@WebServlet("/likedBook")
public class LikedBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LikedBookServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Creating object of like book database class
		LikedBookDatabase likedBookDatabase = new LikedBookDatabase();
		try {
			// Creating object of of HttpSession
			HttpSession httpSession = request.getSession();
			// List of liked book
			List<Book> books = likedBookDatabase.getAllLikedBook((int) httpSession.getAttribute("id"));

			// if liked book list is not empty
			if (!books.isEmpty()) {
				RequestDispatcher rd = request.getRequestDispatcher("likedBookList.jsp");
				request.setAttribute("books", books);
				rd.forward(request, response);
			} else {
				// if liked book list is empty
				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				request.setAttribute("likedMsg", "Empty liked book list!");
				rd.forward(request, response);
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}
}
