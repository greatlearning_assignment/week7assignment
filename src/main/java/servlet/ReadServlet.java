package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;

import javax.servlet.http.HttpSession;

import database.LikedBookDatabase;
import database.ReadBookDatabase;
import model.LikedBook;
import model.ReadLater;

/**
 * Servlet implementation class ReadServlet
 */
@WebServlet("/read")
public class ReadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReadServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// getting user id from session
		HttpSession httpSession = request.getSession();
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		int userId = (int) httpSession.getAttribute("id");

		// Creating object of read book class
		ReadLater readBook = new ReadLater(bookId, userId);

		// Create object of read database class
		ReadBookDatabase readBookDatabase = new ReadBookDatabase();
		try {
			// checking book is not already in list then insert book into
			if (readBookDatabase.insertReadLater(readBook)) {
				request.setAttribute("readBook", " book is successfully added into read later book List");
				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				rd.forward(request, response);
			} else {
				// if book is already exist in list
				request.setAttribute("readBook", " book is already present in read later book list.");
				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				rd.forward(request, response);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
