package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.BookDatabase;
import model.Book;

/**
 * Servlet implementation class BookList
 */
@WebServlet("/BookList")
public class BooklistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BooklistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Creating object of book database
		System.out.println("booklist");
		BookDatabase bookDatabase = new BookDatabase();
		try {
			// List of all books
			List<Book> books = bookDatabase.getAllBooks();
			System.out.println(books);
			RequestDispatcher rd = request.getRequestDispatcher("bookList.jsp");
			request.setAttribute("books", books);
			rd.forward(request, response);
		} catch (Exception e) {
			e.getStackTrace();
		}
		response.sendRedirect("bookList.jsp");
	}
}
