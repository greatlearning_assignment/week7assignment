package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

import database.LikedBookDatabase;
import model.LikedBook;

/**
 * Servlet implementation class LikedServlet
 */
@WebServlet("/LikedServlet")
public class LikedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LikedServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// getting user id from session
		HttpSession httpSession = request.getSession();
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		int userId = (int) httpSession.getAttribute("id");

		// Creating object of liked book
		LikedBook likedBook = new LikedBook(bookId, userId);
		LikedBookDatabase likedBookDatabase = new LikedBookDatabase();
		try {
			// insert book into list if book is not already exist
			if (likedBookDatabase.insertLikeBook(likedBook)) {
				request.setAttribute("likedBook", "book is added into liked book List");
				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				rd.forward(request, response);
			} else {
				// if book is already in list
				request.setAttribute("likedBook", "book is already present liked book list.");
				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				rd.forward(request, response);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
