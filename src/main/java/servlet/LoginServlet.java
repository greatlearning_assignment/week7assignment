package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;

import javax.servlet.http.HttpSession;

import database.UserDatabase;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// getting all user data
		String email = request.getParameter("email");
		String pwd = request.getParameter("password");
		String register = request.getParameter("register");

		// Create user object of user class
		User user = new User(email, pwd);

		// Creating object of user database class
		UserDatabase userDatabase = new UserDatabase();

		// if user want register then register.jsp will be reflect
		if ((register != null) && (register.equals("Register"))) {
			response.sendRedirect("register.jsp");
		} else {
			// Login functionality code
			try {
				// validating user is correct
				if (userDatabase.validate(user)) {
					HttpSession httpSession = request.getSession();
					httpSession.setAttribute("id", userDatabase.getUserNameByEmail(user.getEmail()).getId());
					httpSession.setAttribute("name", userDatabase.getUserNameByEmail(user.getEmail()).getName());
					RequestDispatcher dispatcher = request.getRequestDispatcher("dashboard");
					dispatcher.forward(request, response);
				} else {
					request.setAttribute("wrongUser", "Email or Password wrong or User are not exist!");
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
