package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

import database.BookDatabase;
import database.ReadBookDatabase;
import model.Book;

/**
 * Servlet implementation class ReadLaterServlet
 */
@WebServlet("/readLater")
public class ReadLaterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReadLaterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Creating object read book database class
		ReadBookDatabase readBookDatabase = new ReadBookDatabase();
		try {
			HttpSession httpSession = request.getSession();
			// List of read later book by user
			List<Book> books = readBookDatabase.getAllLaterReadBook((int) httpSession.getAttribute("id"));

			// If read later list is not empty
			if (!books.isEmpty()) {
				RequestDispatcher rd = request.getRequestDispatcher("readLaterList.jsp");
				request.setAttribute("books", books);
				rd.forward(request, response);
			} else {
				// if read later list is not empty
				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				request.setAttribute("readMsg", "Read later book list is empty");
				rd.forward(request, response);
				response.sendRedirect("dashboard.jsp");
			}
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
